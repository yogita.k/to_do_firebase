import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:todoapp/model/detailtask.dart';

class MyDataBase {
  MyDataBase._();

  static final MyDataBase object = MyDataBase._();
  FirebaseFirestore db = FirebaseFirestore.instance;

  Future<void> addTask({required Task task}) async {
    await db.collection("taskstable").doc(task.id).set(task.toMap());
  }

  Future<List<Task>> getData() async {
    final data = await db.collection('taskstable').get();

    return data.docs.map((e) => Task.fromFirestore(e.data())).toList();
  }

  Future<void> updateData({required String id, required bool isDone}) async {
    await db.collection("taskstable").doc(id).update({"isDone": isDone});
  }

  Future<void> deleteData({required String id}) async {
    return db.collection("taskstable").doc(id).delete();
  }
}
