import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:todoapp/database/my_database.dart';
import 'package:todoapp/model/detailtask.dart';
import 'package:uuid/uuid.dart';

class TaskData extends ChangeNotifier {
  var uuid = const Uuid();
  final MyDataBase db = MyDataBase.object;
  List<Task> _tasks = [];

  Future<void> getData() async {
    _tasks = await db.getData();
    notifyListeners();
  }

  List<Task> get tasks => _tasks;

  int get taskCount => _tasks.length;

  Future<void> addTask({required String title}) async {
    await db.addTask(task: Task(name: title,id: uuid.v4()));
    getData();
  }

  Future<void> updateTask(Task task) async {
     await db.updateData(id: task.id!, isDone: !task.isDone);
    getData();
  }

  Future<void> deleteTask({required String id}) async {
    await db.deleteData(id: id);
    getData();
  }
}
