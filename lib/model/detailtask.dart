class Task {
  String? id;
  String? name;
  bool isDone;

  Task({this.id, this.name, this.isDone = false});

  Task.fromMap(Map<String, dynamic> item)
      : id = item["id"],
        name = item["name"],
        isDone = item["isDone"];

  Map<String, dynamic> toMap() => {
        "id": id,
        "name": name,
        "isDone": isDone,
      };

  factory Task.fromFirestore(Map firestore) => Task(
        id: firestore['id'],
        name: firestore['name'],
       isDone: firestore['isDone'] ,
      );
}
